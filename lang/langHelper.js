import qs from "qs";
import axios from "axios";

export const loadDataByLang = locale =>
  axios({
    method: "POST",
    headers: { "content-type": "application/x-www-form-urlencoded" },
    data: qs.stringify({
      key: "298735498j",
      action: "lang",
      language: locale
    }),
    url: "https://get.lotusstory.org/ajaxurl"
  })
    .then(res => res.data)
    // .catch(() => location.replace("http://localhost:3000/error"));
    .catch(() => location.replace("https://lotusstory.org/error"));


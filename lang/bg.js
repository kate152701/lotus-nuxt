import { loadDataByLang } from "./langHelper";

export default async (context, locale) =>
  await new Promise(resolve => resolve(loadDataByLang("bg")));

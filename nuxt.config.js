export default {
  mode: "universal",
  // Global page headers: https://go.nuxtjs.dev/config-head
  head: {
    // const i18nHead = this.$nuxtI18nHead({ addSeoAttributes: true });
    // return {
    title: "Lotus Fairy Tale",
    htmlAttrs: {
      lang: "en",
    },
    meta: [
      { charset: "utf-8" },
      { name: "viewport", content: "width=device-width, initial-scale=1" },
      { hid: "meta-1", property: "og:locale", content: "en_us" },
      {
        hid: "meta-2",
        content: "IE=edge",
        httpEquiv: "X-UA-Compatible",
      },
      { hid: "meta-8", content: "article", property: "og:type" },
      { hid: "meta-9", content: "482905542113636", property: "fb:app_id" },
      {
        hid: "meta-10",
        content: "https://lotusstory.org/wp-content/uploads/2019/01/05-new.jpg",
        property: "og:image",
      },
      {
        hid: "meta-11",
        content: "https://lotusstory.org/wp-content/uploads/2019/01/05-new.jpg",
        property: "og:image:secure_url",
      },
      { hid: "meta-12", content: "1000", property: "og:image:width" },
      { hid: "meta-13", content: "1000", property: "og:image:height" },
      {
        hid: "meta-14",
        name: "twitter:card",
        content: "summary_large_image",
      },
      {
        hid: "meta-15",
        name: "twitter:image",
        content: "https://lotusstory.org/wp-content/uploads/2019/01/05-new.jpg",
      },
    ],
    link: [{ rel: "icon", type: "image/x-icon", href: "/favicon-lotus.png" }],
    // };
  },
  // Plugins to run before rendering page: https://go.nuxtjs.dev/config-plugins
  plugins: [{ src: "~/plugins/route.js", mode: "client" }],
  // Auto import components: https://go.nuxtjs.dev/config-components
  components: true,
  // Modules for dev and build (recommended): https://go.nuxtjs.dev/config-modules
  buildModules: [
    [
      "nuxt-i18n",
      {
        locales: [
          {
            code: "fr",
            file: "fr.js",
            iso: "fr-FR",
          },
          {
            code: "en",
            file: "en.js",
            iso: "en-US",
          },
          {
            code: "ru",
            file: "ru.js",
          },
          {
            code: "alb",
            file: "alb.js",
          },
          {
            code: "ar",
            file: "ar.js",
          },
          {
            code: "arm",
            file: "arm.js",
          },
          {
            code: "bg",
            file: "bg.js",
          },
          {
            code: "bn",
            file: "bn.js",
          },
          {
            code: "by",
            file: "by.js",
          },
          {
            code: "cs",
            file: "cs.js",
          },
          {
            code: "de",
            file: "de.js",
          },
          {
            code: "dut",
            file: "dut.js",
          },
          {
            code: "el",
            file: "el.js",
          },
          {
            code: "es",
            file: "es.js",
            iso: "es-ES",
          },
          {
            code: "et",
            file: "et.js",
          },
          {
            code: "fa",
            file: "fa.js",
          },
          {
            code: "geo",
            file: "geo.js",
          },
          {
            code: "he",
            file: "he.js",
          },
          {
            code: "hi",
            file: "hi.js",
          },
          {
            code: "hu",
            file: "hu.js",
          },
          {
            code: "id",
            file: "id.js",
          },
          {
            code: "it",
            file: "it.js",
          },
          {
            code: "ja",
            file: "ja.js",
          },
          {
            code: "kk",
            file: "kk.js",
          },
          {
            code: "ko",
            file: "ko.js",
          },
          {
            code: "ky",
            file: "ky.js",
          },
          {
            code: "lt",
            file: "lt.js",
          },
          {
            code: "lv",
            file: "lv.js",
          },
          {
            code: "mn",
            file: "mn.js",
          },
          {
            code: "pl",
            file: "pl.js",
          },
          {
            code: "pt",
            file: "pt.js",
          },
          {
            code: "ro",
            file: "ro.js",
          },
          {
            code: "sv",
            file: "sv.js",
          },
          {
            code: "tr",
            file: "tr.js",
          },
          {
            code: "uk",
            file: "uk.js",
          },
          {
            code: "uz",
            file: "uz.js",
          },
          {
            code: "vi",
            file: "vi.js",
          },
          {
            code: "zh-t",
            file: "zh-t.js",
          },
          {
            code: "zh",
            file: "zh.js",
          },
        ],
        baseUrl: "https://lotusstory.org/",
        detectBrowserLanguage: {
          useCookie: true,
          cookieKey: "language",
          cookieSecure: true,
          alwaysRedirect: true,
          fallbackLocale: "en",
        },
        defaultLocale: "en",
        strategy: "no_prefix",
        langDir: "lang/",
      },
    ],
  ],
  // Modules: https://go.nuxtjs.dev/config-modules
  modules: ["bootstrap-vue/nuxt", "@nuxtjs/axios", "nuxt-lazy-load"],
  css: [
    "@/assets/css/main.scss",
    "@/assets/css/header.scss",
    "@/assets/css/nav.scss",
    "@/assets/css/menu.scss",
    "@/assets/css/langMenu.scss",
    "@/assets/css/footer.scss",
    "@/assets/css/home.scss",
    "@/assets/css/read.scss",
    "@/assets/css/readInternational.scss",
    "@/assets/css/international.scss",
    "@/assets/css/feedback.scss",
    "@/assets/css/watch.scss",
    "@/assets/css/about.scss",
    "@/assets/css/buyTheBook.scss",
  ],

  // Build Configuration: https://go.nuxtjs.dev/config-build
  build: {
    extend(config, ctx) {
      config.module.rules.push({
        test: /\.(ogg|mp3|wav|mpe?g)$/i,
        loader: "file-loader",
        options: {
          name: "[path][name].[ext]",
        },
      });
    },
  },
};

import { getLanguagesList } from "./fetchFunc";

export const state = () => ({
  languages: [],
  dataByLang: {},
  isMusicPlaying: false,
  langGroup: "ltr",
});

export const mutations = {
  storeLanguages(state, data) {
    state.languages = data;
  },
  setLangGroup(state, data) {
    state.langGroup = data;
  },
  toggleMusic(state, data) {
    state.isMusicPlaying = data;
  },
};

export const actions = {
  async prefetchData(store) {
    await getLanguagesList()
      .then((res) => {
        store.commit("storeLanguages", res.data);
      })
      .catch((err) => console.log("err", err));
  },
  setLangGroup(store, langGroup) {
    store.commit("setLangGroup", langGroup);
  },
  playMusic(store) {
    store.commit("toggleMusic", true);
  },
  stopMusic(store) {
    store.commit("toggleMusic", false);
  },
};

export const getters = {
  LANGUAGES_LIST: (s) => s.languages,
  MUSIC_STATUS: (s) => s.isMusicPlaying,
  LANG_GROUP: (s) => s.langGroup,
};

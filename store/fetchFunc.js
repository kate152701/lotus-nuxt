import axios from "axios";
import qs from "qs";

const dataLang = {
  key: "298735498j",
  action: "lang_list",
};

const getLanguagesList = () =>
  axios({
    method: "POST",
    headers: { "content-type": "application/x-www-form-urlencoded" },
    data: qs.stringify(dataLang),
    url: "https://get.lotusstory.org/ajaxurl",
  });

const data = {
  key: "298735498j",
  action: "lang",
};

export { getLanguagesList };

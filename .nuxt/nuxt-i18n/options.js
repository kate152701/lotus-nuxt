import locale77526775 from '../..\\lang\\fr.js'
import locale77427e78 from '../..\\lang\\en.js'
import locale77fcde9e from '../..\\lang\\ru.js'
import locale6a348076 from '../..\\lang\\alb.js'
import locale770bf1f0 from '../..\\lang\\ar.js'
import locale6a8e0d91 from '../..\\lang\\arm.js'
import locale7715095c from '../..\\lang\\bg.js'
import locale771837f5 from '../..\\lang\\bn.js'
import locale771d380a from '../..\\lang\\by.js'
import locale77289551 from '../..\\lang\\cs.js'
import locale77304fa0 from '../..\\lang\\de.js'
import locale6fda0c8a from '../..\\lang\\dut.js'
import locale774195ba from '../..\\lang\\el.js'
import locale7744c453 from '../..\\lang\\es.js'
import locale774538b2 from '../..\\lang\\et.js'
import locale774aad26 from '../..\\lang\\fa.js'
import locale7414d87c from '../..\\lang\\geo.js'
import locale7768ada4 from '../..\\lang\\he.js'
import locale776a7f20 from '../..\\lang\\hi.js'
import locale776ff394 from '../..\\lang\\hu.js'
import locale777650c6 from '../..\\lang\\id.js'
import locale777d96b6 from '../..\\lang\\it.js'
import locale77830b2a from '../..\\lang\\ja.js'
import locale7795ae61 from '../..\\lang\\kk.js'
import locale77977fdd from '../..\\lang\\ko.js'
import locale779c0b93 from '../..\\lang\\ky.js'
import locale77a7dd39 from '../..\\lang\\lt.js'
import locale77a8c5f7 from '../..\\lang\\lv.js'
import locale77b33a80 from '../..\\lang\\mn.js'
import locale77dc9845 from '../..\\lang\\pl.js'
import locale77e03b3d from '../..\\lang\\pt.js'
import locale77fa2464 from '../..\\lang\\ro.js'
import locale780b6a7e from '../..\\lang\\sv.js'
import locale7817b083 from '../..\\lang\\tr.js'
import locale7822996b from '../..\\lang\\uk.js'
import locale78296afc from '../..\\lang\\uz.js'
import locale782fc82e from '../..\\lang\\vi.js'
import locale058ff0e8 from '../..\\lang\\zh-t.js'
import locale7867b1d3 from '../..\\lang\\zh.js'

export const Constants = {
  COMPONENT_OPTIONS_KEY: "nuxtI18n",
  STRATEGIES: {"PREFIX":"prefix","PREFIX_EXCEPT_DEFAULT":"prefix_except_default","PREFIX_AND_DEFAULT":"prefix_and_default","NO_PREFIX":"no_prefix"},
}
export const nuxtOptions = {
  isUniversalMode: true,
  trailingSlash: undefined,
}
export const options = {
  vueI18n: {},
  vueI18nLoader: false,
  locales: [{"code":"fr","file":"fr.js","iso":"fr-FR"},{"code":"en","file":"en.js","iso":"en-US"},{"code":"ru","file":"ru.js"},{"code":"alb","file":"alb.js"},{"code":"ar","file":"ar.js"},{"code":"arm","file":"arm.js"},{"code":"bg","file":"bg.js"},{"code":"bn","file":"bn.js"},{"code":"by","file":"by.js"},{"code":"cs","file":"cs.js"},{"code":"de","file":"de.js"},{"code":"dut","file":"dut.js"},{"code":"el","file":"el.js"},{"code":"es","file":"es.js","iso":"es-ES"},{"code":"et","file":"et.js"},{"code":"fa","file":"fa.js"},{"code":"geo","file":"geo.js"},{"code":"he","file":"he.js"},{"code":"hi","file":"hi.js"},{"code":"hu","file":"hu.js"},{"code":"id","file":"id.js"},{"code":"it","file":"it.js"},{"code":"ja","file":"ja.js"},{"code":"kk","file":"kk.js"},{"code":"ko","file":"ko.js"},{"code":"ky","file":"ky.js"},{"code":"lt","file":"lt.js"},{"code":"lv","file":"lv.js"},{"code":"mn","file":"mn.js"},{"code":"pl","file":"pl.js"},{"code":"pt","file":"pt.js"},{"code":"ro","file":"ro.js"},{"code":"sv","file":"sv.js"},{"code":"tr","file":"tr.js"},{"code":"uk","file":"uk.js"},{"code":"uz","file":"uz.js"},{"code":"vi","file":"vi.js"},{"code":"zh-t","file":"zh-t.js"},{"code":"zh","file":"zh.js"}],
  defaultLocale: "en",
  defaultDirection: "ltr",
  routesNameSeparator: "___",
  defaultLocaleRouteNameSuffix: "default",
  sortRoutes: true,
  strategy: "no_prefix",
  lazy: false,
  langDir: "C:\\Users\\Kate\\Desktop\\work\\lotus-nuxt\\lang",
  rootRedirect: null,
  detectBrowserLanguage: {"alwaysRedirect":true,"cookieCrossOrigin":false,"cookieDomain":null,"cookieKey":"language","cookieSecure":true,"fallbackLocale":"en","onlyOnNoPrefix":false,"onlyOnRoot":false,"useCookie":true},
  differentDomains: false,
  seo: false,
  baseUrl: "https://lotusstory.org/",
  vuex: {"moduleName":"i18n","syncLocale":false,"syncMessages":false,"syncRouteParams":true},
  parsePages: true,
  pages: {},
  skipSettingLocaleOnNavigate: false,
  beforeLanguageSwitch: () => null,
  onBeforeLanguageSwitch: () => {},
  onLanguageSwitched: () => null,
  normalizedLocales: [{"code":"fr","file":"fr.js","iso":"fr-FR"},{"code":"en","file":"en.js","iso":"en-US"},{"code":"ru","file":"ru.js"},{"code":"alb","file":"alb.js"},{"code":"ar","file":"ar.js"},{"code":"arm","file":"arm.js"},{"code":"bg","file":"bg.js"},{"code":"bn","file":"bn.js"},{"code":"by","file":"by.js"},{"code":"cs","file":"cs.js"},{"code":"de","file":"de.js"},{"code":"dut","file":"dut.js"},{"code":"el","file":"el.js"},{"code":"es","file":"es.js","iso":"es-ES"},{"code":"et","file":"et.js"},{"code":"fa","file":"fa.js"},{"code":"geo","file":"geo.js"},{"code":"he","file":"he.js"},{"code":"hi","file":"hi.js"},{"code":"hu","file":"hu.js"},{"code":"id","file":"id.js"},{"code":"it","file":"it.js"},{"code":"ja","file":"ja.js"},{"code":"kk","file":"kk.js"},{"code":"ko","file":"ko.js"},{"code":"ky","file":"ky.js"},{"code":"lt","file":"lt.js"},{"code":"lv","file":"lv.js"},{"code":"mn","file":"mn.js"},{"code":"pl","file":"pl.js"},{"code":"pt","file":"pt.js"},{"code":"ro","file":"ro.js"},{"code":"sv","file":"sv.js"},{"code":"tr","file":"tr.js"},{"code":"uk","file":"uk.js"},{"code":"uz","file":"uz.js"},{"code":"vi","file":"vi.js"},{"code":"zh-t","file":"zh-t.js"},{"code":"zh","file":"zh.js"}],
  localeCodes: ["fr","en","ru","alb","ar","arm","bg","bn","by","cs","de","dut","el","es","et","fa","geo","he","hi","hu","id","it","ja","kk","ko","ky","lt","lv","mn","pl","pt","ro","sv","tr","uk","uz","vi","zh-t","zh"],
}

export const localeMessages = {
  'fr.js': () => Promise.resolve(locale77526775),
  'en.js': () => Promise.resolve(locale77427e78),
  'ru.js': () => Promise.resolve(locale77fcde9e),
  'alb.js': () => Promise.resolve(locale6a348076),
  'ar.js': () => Promise.resolve(locale770bf1f0),
  'arm.js': () => Promise.resolve(locale6a8e0d91),
  'bg.js': () => Promise.resolve(locale7715095c),
  'bn.js': () => Promise.resolve(locale771837f5),
  'by.js': () => Promise.resolve(locale771d380a),
  'cs.js': () => Promise.resolve(locale77289551),
  'de.js': () => Promise.resolve(locale77304fa0),
  'dut.js': () => Promise.resolve(locale6fda0c8a),
  'el.js': () => Promise.resolve(locale774195ba),
  'es.js': () => Promise.resolve(locale7744c453),
  'et.js': () => Promise.resolve(locale774538b2),
  'fa.js': () => Promise.resolve(locale774aad26),
  'geo.js': () => Promise.resolve(locale7414d87c),
  'he.js': () => Promise.resolve(locale7768ada4),
  'hi.js': () => Promise.resolve(locale776a7f20),
  'hu.js': () => Promise.resolve(locale776ff394),
  'id.js': () => Promise.resolve(locale777650c6),
  'it.js': () => Promise.resolve(locale777d96b6),
  'ja.js': () => Promise.resolve(locale77830b2a),
  'kk.js': () => Promise.resolve(locale7795ae61),
  'ko.js': () => Promise.resolve(locale77977fdd),
  'ky.js': () => Promise.resolve(locale779c0b93),
  'lt.js': () => Promise.resolve(locale77a7dd39),
  'lv.js': () => Promise.resolve(locale77a8c5f7),
  'mn.js': () => Promise.resolve(locale77b33a80),
  'pl.js': () => Promise.resolve(locale77dc9845),
  'pt.js': () => Promise.resolve(locale77e03b3d),
  'ro.js': () => Promise.resolve(locale77fa2464),
  'sv.js': () => Promise.resolve(locale780b6a7e),
  'tr.js': () => Promise.resolve(locale7817b083),
  'uk.js': () => Promise.resolve(locale7822996b),
  'uz.js': () => Promise.resolve(locale78296afc),
  'vi.js': () => Promise.resolve(locale782fc82e),
  'zh-t.js': () => Promise.resolve(locale058ff0e8),
  'zh.js': () => Promise.resolve(locale7867b1d3),
}

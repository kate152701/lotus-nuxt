import Vue from 'vue'
import Router from 'vue-router'
import { normalizeURL, decode } from 'ufo'
import { interopDefault } from './utils'
import scrollBehavior from './router.scrollBehavior.js'

const _0c691aed = () => interopDefault(import('..\\pages\\about.vue' /* webpackChunkName: "pages/about" */))
const _5643b7f8 = () => interopDefault(import('..\\pages\\access-denied.vue' /* webpackChunkName: "pages/access-denied" */))
const _169f8f08 = () => interopDefault(import('..\\pages\\audio-player.vue' /* webpackChunkName: "pages/audio-player" */))
const _7b6c5e7c = () => interopDefault(import('..\\pages\\buy-the-book.vue' /* webpackChunkName: "pages/buy-the-book" */))
const _6fe713e8 = () => interopDefault(import('..\\pages\\error.vue' /* webpackChunkName: "pages/error" */))
const _502d4956 = () => interopDefault(import('..\\pages\\feedback.vue' /* webpackChunkName: "pages/feedback" */))
const _300c1634 = () => interopDefault(import('..\\pages\\read.vue' /* webpackChunkName: "pages/read" */))
const _4b884b2f = () => interopDefault(import('..\\pages\\watch.vue' /* webpackChunkName: "pages/watch" */))
const _2c20f5b2 = () => interopDefault(import('..\\pages\\index.vue' /* webpackChunkName: "pages/index" */))

const emptyFn = () => {}

Vue.use(Router)

export const routerOptions = {
  mode: 'history',
  base: '/',
  linkActiveClass: 'nuxt-link-active',
  linkExactActiveClass: 'nuxt-link-exact-active',
  scrollBehavior,

  routes: [{
    path: "/about",
    component: _0c691aed,
    name: "about"
  }, {
    path: "/access-denied",
    component: _5643b7f8,
    name: "access-denied"
  }, {
    path: "/audio-player",
    component: _169f8f08,
    name: "audio-player"
  }, {
    path: "/buy-the-book",
    component: _7b6c5e7c,
    name: "buy-the-book"
  }, {
    path: "/error",
    component: _6fe713e8,
    name: "error"
  }, {
    path: "/feedback",
    component: _502d4956,
    name: "feedback"
  }, {
    path: "/read",
    component: _300c1634,
    name: "read"
  }, {
    path: "/watch",
    component: _4b884b2f,
    name: "watch"
  }, {
    path: "/",
    component: _2c20f5b2,
    name: "index"
  }],

  fallback: false
}

export function createRouter (ssrContext, config) {
  const base = (config._app && config._app.basePath) || routerOptions.base
  const router = new Router({ ...routerOptions, base  })

  // TODO: remove in Nuxt 3
  const originalPush = router.push
  router.push = function push (location, onComplete = emptyFn, onAbort) {
    return originalPush.call(this, location, onComplete, onAbort)
  }

  const resolve = router.resolve.bind(router)
  router.resolve = (to, current, append) => {
    if (typeof to === 'string') {
      to = normalizeURL(to)
    }
    return resolve(to, current, append)
  }

  return router
}

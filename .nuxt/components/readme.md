# Discovered Components

This is an auto-generated list of components discovered by [nuxt/components](https://github.com/nuxt/components).

You can directly use them in pages and other components without the need to import them.

**Tip:** If a component is conditionally rendered with `v-if` and is big, it is better to use `Lazy` or `lazy-` prefix to lazy load.

- `<DesktopPage>` | `<desktop-page>` (components/DesktopPage.vue)
- `<Footer>` | `<footer>` (components/Footer.vue)
- `<Header>` | `<header>` (components/Header.vue)
- `<LangMenu>` | `<lang-menu>` (components/LangMenu.vue)
- `<LittleHeader>` | `<little-header>` (components/LittleHeader.vue)
- `<Menu>` | `<menu>` (components/Menu.vue)
- `<MobileContentSlide>` | `<mobile-content-slide>` (components/MobileContentSlide.vue)
- `<Nav>` | `<nav>` (components/Nav.vue)
- `<ReadHeader>` | `<read-header>` (components/ReadHeader.vue)

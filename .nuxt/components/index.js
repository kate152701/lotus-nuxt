export { default as DesktopPage } from '../..\\components\\DesktopPage.vue'
export { default as Footer } from '../..\\components\\Footer.vue'
export { default as Header } from '../..\\components\\Header.vue'
export { default as LangMenu } from '../..\\components\\LangMenu.vue'
export { default as LittleHeader } from '../..\\components\\LittleHeader.vue'
export { default as Menu } from '../..\\components\\Menu.vue'
export { default as MobileContentSlide } from '../..\\components\\MobileContentSlide.vue'
export { default as Nav } from '../..\\components\\Nav.vue'
export { default as ReadHeader } from '../..\\components\\ReadHeader.vue'

// nuxt/nuxt.js#8607
function wrapFunctional(options) {
  if (!options || !options.functional) {
    return options
  }

  const propKeys = Array.isArray(options.props) ? options.props : Object.keys(options.props || {})

  return {
    render(h) {
      const attrs = {}
      const props = {}

      for (const key in this.$attrs) {
        if (propKeys.includes(key)) {
          props[key] = this.$attrs[key]
        } else {
          attrs[key] = this.$attrs[key]
        }
      }

      return h(options, {
        on: this.$listeners,
        attrs,
        props,
        scopedSlots: this.$scopedSlots,
      }, this.$slots.default)
    }
  }
}

export default ({ app }) => {
  // Every time the route changes (fired on initialization too)
  app.router.beforeEach((to, from, next) => {
    if (to.name !== from.name) {
      if (to.meta && to.meta.title) document.title = to.meta.title;
    }
    next();
  });
};

#  first install 
$ npm i
$ npm i -g nuxt
$ npm i pm2@latest -g
$ nuxt build
$ pm2 start
$ forever start server

#  update 
$ git add .
$ git commit -m "before merge"
$ git pull
$ nuxt build
$ pm2 start
$ forever start server

For detailed explanation on how things work, check out [Nuxt.js docs](https://nuxtjs.org).
